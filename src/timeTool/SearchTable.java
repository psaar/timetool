package timeTool;

/**
 * Setters and getters for search results table
 */
public class SearchTable {

    private String area;
    private String time;

    public SearchTable(){
        this.area = "";
        this.time = "";
    }

    public SearchTable(String area, String time){
        this.area = area;
        this.time = time;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
