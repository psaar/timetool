package timeTool;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Contains methods for returning time in a specific timezone and local time.
 * Also houses the array of locations.
 */
public class Zones {

	/**
	 * Returns the time in a specified area, DST is already correct
	 * @param area Specific string value wich TimeZone.getTimeZone() can use
	 * @return Time in hh:mm:ss format for the region provided as a string
	 */
	public static String timezone(String area) {
		// Create a calendar object for representing a selected time zone.
		Calendar selectTime = new GregorianCalendar(TimeZone.getTimeZone(area));

		// create variables to house the time values
		int hour = selectTime.get(Calendar.HOUR_OF_DAY);
		int minute = selectTime.get(Calendar.MINUTE);
		int second = selectTime.get(Calendar.SECOND);

		// use time variables to build the string
		return String.format("%02d:%02d:%02d", hour, minute, second);
	}

	/**
	 * Gets local time
	 * @return Local time in a hh:mm:ss string value
	 */
	public static String localTime() {
		return String.format("Local time: %tT", Calendar.getInstance());
	}

	// put all available areas into a 2D array to be used in GUI.java
	static String[][] zones = new String[][] {
			{ "GMT-11", "Pacific/Apia", "Pacific/Midway", "Pacific/Niue", "Pacific/Pago_Pago", "Pacific/Samoa",
					"US/Samoa", "America/Adak", "America/Atka" },
			{ "GMT-10", "Pacific/Fakaofo", "Pacific/Honolulu", "Pacific/Johnston", "Pacific/Rarotonga",
					"Pacific/Tahiti", "US/Aleutian", "US/Hawaii", "Pacific/Marquesas", "America/Anchorage",
					"America/Juneau", "America/Nome", "America/Yakutat" },
			{ "GMT-9", "Pacific/Gambier", "US/Alaska", "America/Dawson", "America/Ensenada", "America/Los_Angeles",
					"America/Tijuana", "America/Vancouver", "America/Whitehorse", "Canada/Pacific", "Canada/Yukon" },
			{ "GMT-8", "Mexico/BajaNorte", "Pacific/Pitcairn", "US/Pacific", "US/Pacific-New", "America/Boise",
					"America/Cambridge_Bay", "America/Chihuahua", "America/Dawson_Creek", "America/Denver",
					"America/Edmonton", "America/Hermosillo", "America/Inuvik", "America/Mazatlan", "America/Phoenix",
					"America/Shiprock", "America/Yellowknife", "Canada/Mountain" },
			{ "GMT-7", "Mexico/BajaSur", "Navajo", "US/Arizona", "US/Mountain", "America/Belize", "America/Cancun",
					"America/Chicago", "America/Costa_Rica", "America/El_Salvador", "America/Guatemala",
					"America/Managua", "America/Menominee", "America/Merida", "America/Mexico_City",
					"America/Monterrey", "America/North_Dakota/Center", "America/Rainy_River", "America/Rankin_Inlet",
					"America/Regina", "America/Swift_Current", "America/Tegucigalpa", "America/Winnipeg",
					"Canada/Central", "Canada/East-Saskatchewan", "Canada/Saskatchewan", "Chile/EasterIsland" },
			{ "GMT-6", "Mexico/General", "Pacific/Easter", "Pacific/Galapagos", "America/Bogota", "America/Cayman",
					"America/Detroit", "America/Eirunepe", "America/Fort_Wayne", "America/Grand_Turk",
					"America/Guayaquil", "America/Havana", "America/Indiana/Indianapolis", "America/Indiana/Knox",
					"America/Indiana/Marengo", "America/Indiana/Vevay", "America/Indianapolis", "America/Iqaluit",
					"America/Jamaica", "America/Kentucky/Louisville", "America/Kentucky/Monticello", "America/Knox_IN",
					"America/Lima", "America/Louisville", "America/Montreal", "America/Nassau", "America/New_York",
					"America/Nipigon", "America/Panama", "America/Pangnirtung", "America/Port-au-Prince",
					"America/Porto_Acre", "America/Rio_Branco", "America/Thunder_Bay", "America/Toronto", "Brazil/Acre",
					"Canada/Eastern", "Cuba" },
			{ "GMT-5", "Jamaica", "US/East-Indiana", "US/Eastern", "US/Indiana-Starke", "US/Michigan",
					"America/Anguilla", "America/Antigua", "America/Aruba", "America/Asuncion", "America/Barbados",
					"America/Boa_Vista", "America/Campo_Grande", "America/Caracas", "America/Cuiaba", "America/Curacao",
					"America/Dominica", "America/Glace_Bay", "America/Goose_Bay", "America/Grenada",
					"America/Guadeloupe", "America/Guyana", "America/Halifax", "America/La_Paz", "America/Manaus",
					"America/Martinique", "America/Montserrat", "America/Port_of_Spain", "America/Porto_Velho",
					"America/Puerto_Rico", "America/Santiago", "America/Santo_Domingo", "America/St_Kitts",
					"America/St_Lucia", "America/St_Thomas", "America/St_Vincent", "America/Thule", "America/Tortola",
					"America/Virgin", "Antarctica/Palmer", "Atlantic/Bermuda", "Atlantic/Stanley", "Brazil/West",
					"Canada/Atlantic", "Chile/Continental" },
			{ "GMT-4", "America/St_Johns", "Canada/Newfoundland", "America/Araguaina", "America/Bahia", "America/Belem",
					"America/Buenos_Aires", "America/Catamarca", "America/Cayenne", "America/Cordoba",
					"America/Fortaleza", "America/Godthab", "America/Jujuy", "America/Maceio", "America/Mendoza",
					"America/Miquelon", "America/Montevideo", "America/Paramaribo", "America/Recife", "America/Rosario",
					"America/Sao_Paulo", "Antarctica/Rothera", "Brazil/East" },
			{ "GMT-3", "America/Noronha", "Atlantic/South_Georgia", "Brazil/DeNoronha" },
			{ "GMT-2", "America/Scoresbysund", "Atlantic/Azores", "Atlantic/Cape_Verde" },
			{ "GMT-1", "Africa/Abidjan", "Africa/Accra", "Africa/Bamako", "Africa/Banjul", "Africa/Bissau",
					"Africa/Casablanca", "Africa/Conakry", "Africa/Dakar", "Africa/El_Aaiun", "Africa/Freetown",
					"Africa/Lome", "Africa/Monrovia", "Africa/Nouakchott", "Africa/Ouagadougou", "Africa/Sao_Tome",
					"Africa/Timbuktu", "America/Danmarkshavn", "Atlantic/Canary", "Atlantic/Faeroe", "Atlantic/Madeira",
					"Atlantic/Reykjavik", "Atlantic/St_Helena", "Eire" },
			{ "GMT 0", "Europe/Belfast", "Europe/Dublin", "Europe/Lisbon", "Europe/London", "Greenwich", "Iceland",
					"Portugal", "Africa/Algiers", "Africa/Bangui", "Africa/Brazzaville", "Africa/Ceuta",
					"Africa/Douala", "Africa/Kinshasa", "Africa/Lagos", "Africa/Libreville", "Africa/Luanda",
					"Africa/Malabo", "Africa/Ndjamena", "Africa/Niamey", "Africa/Porto-Novo", "Africa/Tunis",
					"Africa/Windhoek", "Arctic/Longyearbyen", "Atlantic/Jan_Mayen" },
			{ "GMT+1", "Europe/Amsterdam", "Europe/Andorra", "Europe/Belgrade", "Europe/Berlin", "Europe/Bratislava",
					"Europe/Brussels", "Europe/Budapest", "Europe/Copenhagen", "Europe/Gibraltar", "Europe/Ljubljana",
					"Europe/Luxembourg", "Europe/Madrid", "Europe/Malta", "Europe/Monaco", "Europe/Oslo",
					"Europe/Paris", "Europe/Prague", "Europe/Rome", "Europe/San_Marino", "Europe/Sarajevo",
					"Europe/Skopje", "Europe/Stockholm", "Europe/Tirane", "Europe/Vaduz", "Europe/Vatican",
					"Europe/Vienna", "Europe/Warsaw", "Europe/Zagreb", "Europe/Zurich", "Poland", "Africa/Blantyre",
					"Africa/Bujumbura", "Africa/Cairo", "Africa/Gaborone", "Africa/Harare", "Africa/Johannesburg",
					"Africa/Kigali", "Africa/Lubumbashi", "Africa/Lusaka", "Africa/Maputo", "Africa/Maseru",
					"Africa/Mbabane", "Africa/Tripoli", "Asia/Amman", "Asia/Beirut", "Asia/Damascus", "Asia/Gaza",
					"Asia/Istanbul", "Asia/Jerusalem", "Asia/Nicosia", "Asia/Tel_Aviv", "Egypt" },
			{ "GMT+2", "Europe/Athens", "Europe/Bucharest", "Europe/Chisinau", "Europe/Helsinki", "Europe/Istanbul",
					"Europe/Kaliningrad", "Europe/Kiev", "Europe/Minsk", "Europe/Nicosia", "Europe/Riga",
					"Europe/Simferopol", "Europe/Sofia", "Europe/Tallinn", "Europe/Tiraspol", "Europe/Uzhgorod",
					"Europe/Vilnius", "Europe/Zaporozhye", "Israel", "Libya", "Turkey", "Africa/Addis_Ababa",
					"Africa/Asmera", "Africa/Dar_es_Salaam", "Africa/Djibouti", "Africa/Kampala", "Africa/Khartoum",
					"Africa/Mogadishu", "Africa/Nairobi", "Antarctica/Syowa", "Asia/Aden", "Asia/Baghdad",
					"Asia/Bahrain", "Asia/Kuwait", "Asia/Qatar", "Asia/Riyadh" },
			{ "GMT+3", "Europe/Moscow", "Indian/Antananarivo", "Indian/Comoro", "Indian/Mayotte", "Asia/Tehran", "Iran",
					"Asia/Aqtau", "Asia/Baku", "Asia/Dubai", "Asia/Muscat", "Asia/Oral", "Asia/Tbilisi",
					"Asia/Yerevan" },
			{ "GMT+4", "Europe/Samara", "Indian/Mahe", "Indian/Mauritius", "Indian/Reunion", "Asia/Kabul",
					"Asia/Aqtobe", "Asia/Ashgabat", "Asia/Ashkhabad", "Asia/Bishkek", "Asia/Dushanbe", "Asia/Karachi",
					"Asia/Samarkand", "Asia/Tashkent", "Asia/Yekaterinburg" },
			{ "GMT+5", "Indian/Kerguelen", "Indian/Maldives", "Asia/Calcutta", "Asia/Katmandu", "Antarctica/Mawson",
					"Antarctica/Vostok", "Asia/Almaty", "Asia/Colombo", "Asia/Dacca", "Asia/Dhaka", "Asia/Novosibirsk",
					"Asia/Omsk", "Asia/Qyzylorda", "Asia/Thimbu", "Asia/Thimphu" },
			{ "GMT+6", "Indian/Chagos", "Asia/Rangoon", "Indian/Cocos", "Antarctica/Davis", "Asia/Bangkok", "Asia/Hovd",
					"Asia/Jakarta", "Asia/Krasnoyarsk", "Asia/Phnom_Penh", "Asia/Pontianak", "Asia/Saigon",
					"Asia/Vientiane" },
			{ "GMT+7", "Indian/Christmas", "Antarctica/Casey", "Asia/Brunei", "Asia/Chongqing", "Asia/Chungking",
					"Asia/Harbin", "Asia/Hong_Kong", "Asia/Irkutsk", "Asia/Kashgar", "Asia/Kuala_Lumpur",
					"Asia/Kuching", "Asia/Macao", "Asia/Macau", "Asia/Makassar", "Asia/Manila", "Asia/Shanghai",
					"Asia/Singapore", "Asia/Taipei", "Asia/Ujung_Pandang", "Asia/Ulaanbaatar", "Asia/Ulan_Bator",
					"Asia/Urumqi", "Australia/Perth", "Australia/West" },
			{ "GMT+8", "Hongkong", "Singapore", "Asia/Choibalsan", "Asia/Dili", "Asia/Jayapura", "Asia/Pyongyang",
					"Asia/Seoul", "Asia/Tokyo", "Asia/Yakutsk" },
			{ "GMT+9", "Japan", "Pacific/Palau", "Australia/Adelaide", "Australia/Broken_Hill", "Australia/Darwin",
					"Australia/North", "Australia/South", "Australia/Yancowinna", "Antarctica/DumontDUrville",
					"Asia/Sakhalin", "Asia/Vladivostok", "Australia/ACT", "Australia/Brisbane", "Australia/Canberra",
					"Australia/Hobart", "Australia/Lindeman", "Australia/Melbourne", "Australia/NSW",
					"Australia/Queensland", "Australia/Sydney", "Australia/Tasmania", "Australia/Victoria" },
			{ "GMT+10", "Pacific/Guam", "Pacific/Port_Moresby", "Pacific/Saipan", "Pacific/Truk", "Pacific/Yap",
					"Australia/LHI", "Australia/Lord_Howe", "Asia/Magadan" },
			{ "GMT+11", "Pacific/Efate", "Pacific/Guadalcanal", "Pacific/Kosrae", "Pacific/Noumea", "Pacific/Ponape",
					"Pacific/Norfolk", "Antarctica/McMurdo", "Antarctica/South_Pole", "Asia/Anadyr", "Asia/Kamchatka" },
			{ "GMT+12", "Kwajalein", "Pacific/Auckland", "Pacific/Fiji", "Pacific/Funafuti", "Pacific/Kwajalein",
					"Pacific/Majuro", "Pacific/Nauru", "Pacific/Tarawa", "Pacific/Wake", "Pacific/Wallis",
					"Pacific/Chatham" },
			{ "GMT+13", "Pacific/Enderbury", "Pacific/Tongatapu" },
			{ "GMT+14", "Pacific/Kiritimati" } };
}
