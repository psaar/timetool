package timeTool;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.util.Duration;
import timeTool.Zones;

/**
 * A tool to view the time in different areas of the world and compare it to the local time
 * 
 * @version 1.0
 * @author Priit Saar
 * @since 0.1
 */
public class GUI extends Application {

	// create the necessary elements that will be used to build GUI
	Stage window;
	Scene scene;
	Button button;
	Button searchButton;
	Button help;
	ComboBox<String> choiceGMT;
	ComboBox<String> choiceArea;
	Label localTime;
	Label choiceTime;
	Timeline timelineChoice;
	Timeline timelineLocal;
	TreeView<String> treeLeft;
	TextField search;
	TableView<SearchTable> table;

	/** main method */
	public static void main(String[] args) {
		launch(args);
	}

	/** contains everything related to layout and interactable fields/buttons*/
	@SuppressWarnings("unchecked")//compiler doesn't like arrays of generic types
	@Override
	public void start(Stage primaryStage) throws Exception {

		// specify all visible/interactable elements
		window = primaryStage;
		window.setTitle("Time Tool");
		button = new Button("Show time");
		searchButton = new Button("Search");
		help = new Button("Help");
		choiceGMT = new ComboBox<>();
		choiceArea = new ComboBox<>();
		localTime = new Label();
		choiceTime = new Label("Area not selected");
		timelineChoice = new Timeline();
		search = new TextField();
		search.setPromptText("Search for an area");
		search.setAlignment(Pos.CENTER);

		/* Root of tree, expanded */
		TreeItem<String> root;
		root = new TreeItem<>();
		root.setExpanded(true);

		/* Start the localtime clock indefinetly */
		timelineLocal = new Timeline(new KeyFrame(Duration.seconds(0), event -> localTime.setText(Zones.localTime())),
				new KeyFrame(Duration.seconds(1)));
		timelineLocal.setCycleCount(Animation.INDEFINITE);
		timelineLocal.play();

		/* Add all timezones to the combobox and also populate the treeview */
		for (int i = 0; i < Zones.zones.length; i++) {
			choiceGMT.getItems().add(Zones.zones[i][0]);
			makeBranch(Zones.zones[i][0], root);
			for (int j = 1; j < Zones.zones[i].length; j++) {
				makeBranch(replaceChar(Zones.zones[i][j], '_', ' '), root.getChildren().get(i));
			}
		}

		// set comboboxes default explanatory text
		choiceGMT.setPromptText("Choose the timezone to view");
		choiceArea.setPromptText("Choose the area to view");
		
		// Tell the buttons what to do on click
		choiceGMT.setOnAction(e -> selectTimezone());
		button.setOnAction(e -> selectClick());
		searchButton.setOnAction(e -> searchClick());
		help.setOnAction(e -> Alert.helpWindow());
		

		// Create the tree, hide the main Root and create listener for tree selection
		treeLeft = new TreeView<>(root);
		treeLeft.setShowRoot(false);
		treeLeft.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> {
			for (int i = 0; i < Zones.zones.length; i++) {
				if (newValue != null) {
					treeClick(newValue);
				}
			}
		});

		// Area column
		TableColumn<SearchTable, String> areaColumn = new TableColumn<>("Area");
		areaColumn.setMinWidth(200);
		areaColumn.setCellValueFactory(new PropertyValueFactory<>("area"));

		// Time column
		TableColumn<SearchTable, String> timeColumn = new TableColumn<>("Time");
		timeColumn.setMinWidth(100);
		timeColumn.setCellValueFactory(new PropertyValueFactory<>("time"));

		// Create the table and add the columns to it
		table = new TableView<>();
		table.getColumns().addAll(areaColumn, timeColumn);
		
		// Create listener for the table which activates the resultClick method when something is selected
		table.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
		    if (newSelection != null) {
		        resultClick();
		    }
		});

		// Create layouts
		VBox layoutCenter = new VBox(15);
		layoutCenter.getChildren().addAll(choiceGMT, choiceArea, button, localTime, choiceTime, help);
		layoutCenter.setAlignment(Pos.CENTER);

		HBox layoutRightTop = new HBox(4);
		layoutRightTop.setPadding(new Insets(20, 10, 10, 10));
		layoutRightTop.getChildren().addAll(search, searchButton);
		search.setPrefWidth(200);
		searchButton.setMinWidth(75);

		HBox layoutRightBottom = new HBox(4);
		layoutRightBottom.setPadding(new Insets(1, 10, 10, 1));
		layoutRightBottom.getChildren().add(table);
		layoutRightBottom.setAlignment(Pos.BOTTOM_CENTER);

		// Right side VBox that holds the search textfield, button and results table
		VBox layoutRight = new VBox();
		layoutRight.getChildren().addAll(layoutRightTop, layoutRightBottom);

		// Create borderpane to hold the layouts and set their locations
		BorderPane borderPane = new BorderPane();
		borderPane.setCenter(layoutCenter);
		borderPane.setLeft(treeLeft);
		borderPane.setRight(layoutRight);

		// Set the scene and show window
		scene = new Scene(borderPane, 850, 250);
		window.setScene(scene);
		window.setResizable(false);
		window.show();
	}

	/**
	 * Finds a match for a chosen string from a 2D array of strings and returns
	 * the first results index. Chosen string must be in the first slot of the
	 * sub array unless specified otherwise
	 * 
	 * @param array Array to look through
	 * @param value String to look for
	 */
	public int find(String[][] array, String value) {
		int i = 0;
		for (i = 0; i < array.length; i++) {
			if (array[i][0] == value) {
				break;
			}
		}
		return i;
	}

	/**
	 * Replaces all occurrences of a character in a string.
	 * 
	 * @param s input string
	 * @param orig character to replace
	 * @param with character to replace with
	 */
	public String replaceChar(String s, char orig, char with) {
		char[] str = s.toCharArray();
		for (int i = 0; i < str.length; i++) {
			if (str[i] == orig) {
				str[i] = with;
			}
		}
		return new String(str);
	}

	/**
	 * Create branches for treeview
	 * 
	 * @param title Name of new branch
	 * @param parent Parent branch
	 * @return New tree branch
	 */
	public TreeItem<String> makeBranch(String title, TreeItem<String> parent) {
		TreeItem<String> item = new TreeItem<>(title);
		item.setExpanded(false);
		parent.getChildren().add(item);
		return item;
	}

	/**
	 * When given a treeitem it gets the time for that treeitem
	 * @param treeItem Selected area to get the time for
	 */
	public void treeClick(TreeItem<String> treeItem) {
		timelineChoice.stop();
		timelineChoice = new Timeline(
				new KeyFrame(Duration.seconds(0),
						event -> choiceTime.setText(treeItem.getValue() + ": "
								+ Zones.timezone(replaceChar(treeItem.getValue(), ' ', '_')))),
				new KeyFrame(Duration.seconds(1)));
		timelineChoice.setCycleCount(Animation.INDEFINITE);
		timelineChoice.play();
	}
	/**
	 * When a GMT timezone is selected the areas combobox gets repopulated and
	 * previusly running timeline is stopped.
	 */
	public void selectTimezone() {
		// stop the timeline when new timezone is selected
		if (timelineChoice.getStatus() == Animation.Status.RUNNING) {
			timelineChoice.stop();
		}
		// uses find method to get array index
		int gmtLoc = find(Zones.zones, choiceGMT.getValue());

		// clear previous comboBox values and populate it with new ones
		choiceArea.getItems().clear();
		for (int i = 1; i < Zones.zones[gmtLoc].length; i++) {
			choiceArea.getItems().add(replaceChar(Zones.zones[gmtLoc][i], '_', ' '));
		}
		// every time new timezone is selected set the time as "Area not selected"
		choiceTime.setText("Area not selected");
	}

	/**
	 * Display time in chosen area. Uses timeline to continuously update the counter every second
	 */
	public void selectClick() {
		// if both options are selected display and start the time otherwise throw error window
		if ((choiceGMT.getValue() == null) || (choiceArea.getValue() == null)) {
			Alert.popup("No selection", "You need to select an area first");
		} else {
			timelineChoice.stop();
			timelineChoice = new Timeline(
					new KeyFrame(Duration.seconds(0),
							event -> choiceTime.setText(choiceArea.getValue() + ": "
									+ Zones.timezone(replaceChar(choiceArea.getValue(), ' ', '_')))),
					new KeyFrame(Duration.seconds(1)));
			timelineChoice.setCycleCount(Animation.INDEFINITE);
			timelineChoice.play();
		}
	}

	/**
	 * Search the zones array for all the values that match the one in searchbox
	 * and populates the results table with them
	 */
	public void searchClick() {
		// if the timeline is running stop it
		if (timelineChoice.getStatus() == Animation.Status.RUNNING) {
			timelineChoice.stop();
		}
		// reset the clock and clear the table then repopulate it
		choiceTime.setText("Area not selected");
		table.getItems().clear();
		table.getSelectionModel().clearSelection();
		for (int i = 0; i < Zones.zones.length; i++) {
			for (int j = 1; j < Zones.zones[i].length; j++) {
				if (Zones.zones[i][j].toLowerCase().contains(search.getText())) {
					SearchTable item = new SearchTable();
					item.setArea(replaceChar(Zones.zones[i][j], '_', ' '));
					item.setTime(Zones.zones[i][0]);
					table.getItems().add(item);
				}
			}
		}
		// clear the searchbox
		search.clear();
	}
	
	/**
	 * When value in table of results is clicked show the time for that area
	 */
	public void resultClick() {
		// if the timeline is running stop it
		SearchTable areaName = table.getSelectionModel().getSelectedItem();
		if (timelineChoice.getStatus() == Animation.Status.RUNNING) {
			timelineChoice.stop();
		}
		// start timeline with new area
		timelineChoice = new Timeline(
				new KeyFrame(Duration.seconds(0),
						event -> choiceTime.setText(areaName.getArea() + ": "
								+ Zones.timezone(replaceChar(areaName.getArea(), ' ', '_')))),
				new KeyFrame(Duration.seconds(1)));
		timelineChoice.setCycleCount(Animation.INDEFINITE);
		timelineChoice.play();
	}
} //END OF CLASS