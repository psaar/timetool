package timeTool;

import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.text.TextAlignment;
import javafx.scene.control.*;
import javafx.stage.*;
import javafx.geometry.*;

public class Alert {

	/**
	 * Create the alert window when time can not be retrieved due to unselected data
	 * @param title Title of the popup window
	 * @param message Information about the popup
	 */
	public static void popup(String title, String message) {
		Stage window = new Stage();
		
		//Modality - alert window on top, has to be dismissed
		window.initModality(Modality.APPLICATION_MODAL);	
		window.setTitle(title);
		window.setMinWidth(280);
		window.setMinHeight(100);
		
		Label label = new Label();
		label.setText(message);
		
		Button ok = new Button("Ok");
		ok.setOnAction(e -> window.close());
		
		VBox layout = new VBox(10);
		layout.getChildren().addAll(label, ok);
		layout.setAlignment(Pos.CENTER);
		
		Scene scene = new Scene(layout);
		window.setScene(scene);
		window.setResizable(false);
		window.showAndWait();
	}
	
	/**
	 * Help information window
	 */
	public static void helpWindow() {
		Stage window = new Stage();
		
		//Modality - alert window on top, has to be dismissed
		window.initModality(Modality.APPLICATION_MODAL);	
		window.setTitle("About the tool and timezones");
		window.setMinWidth(400);
		window.setMinHeight(250);
		
		//Program explanation text
		Label label1 = new Label();
		label1.setText("Left side shows time automatically when you select something. Middle shows time if you\n"
				+ "select both timezone and area and click the button, but you can update the time for the areas\n"
				+ "in the same timezone simply by swiching the area. If you select another zone the time is reset.\n"
				+ "Right side has a search function for areas. You can look for cities or continents/oceans.\n"
				+ "The tool automatically detects daylight savings time.");
		label1.setTextAlignment(TextAlignment.CENTER);
		
		//Some timezones information
		Label label2 = new Label();
		label2.setText("GMT or Greenwich Mean Time is the same as UTC or\n"
				+ "Coordinated Universal time. PST or Pacific Standard Time is\n"
				+ "8 hours behind GMT. EST or Eastern Standard Time is 5 hours behind GMT.");
		label2.setTextAlignment(TextAlignment.CENTER);
		
		Button ok = new Button("Got it");
		ok.setOnAction(e -> window.close());
		
		VBox layout = new VBox(10);
		layout.getChildren().addAll(label1, label2, ok);
		layout.setAlignment(Pos.CENTER);
		
		Scene scene = new Scene(layout);
		window.setScene(scene);
		window.setResizable(false);
		window.showAndWait();
	}
}
